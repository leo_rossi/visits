drop table if exists visit;

create table visit (
    id identity primary key,
    name varchar(50) not null,
    visit_date date not null,
    message varchar(500)
);