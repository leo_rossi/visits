package br.edu.ifsp.visitas.entity;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

public class VisitEntity {

    private Long id;

    @NotBlank(message = "Nome é obrigatório")
    private String name;

    private LocalDate date;

    private String message;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "VisitEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", date=" + date +
                ", message='" + message + '\'' +
                '}';
    }
}
