package br.edu.ifsp.visitas.repository;

import br.edu.ifsp.visitas.entity.VisitEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

@Repository
public class VisitRepositoryJdbcImpl implements VisitRepository {

    private static final String INSERT_QUERY = " insert into " +
            " visit (name, visit_date, message) " +
            " values (?, ?, ?)";
    private static final String SELECT_ALL_QUERY = " select * from visit order by visit_date desc";
    private static final String DELETE_QUERY = " delete from visit where id = ?";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public VisitEntity save(final VisitEntity visitEntity) {
        Objects.requireNonNull(visitEntity, "visitEntity can't be null!");
        Objects.requireNonNull(visitEntity.getName(), "visitEntity.name can't be null!" );

        visitEntity.setDate(LocalDate.now());

        final KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(conn -> {
            final PreparedStatement ps = conn.prepareStatement(INSERT_QUERY, PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, visitEntity.getName());
            ps.setDate(2, Date.valueOf(visitEntity.getDate()));
            ps.setString(3, visitEntity.getMessage());
            return ps;
        }, keyHolder);

        final long generatedId = Objects.requireNonNull(keyHolder.getKey()).longValue();
        visitEntity.setId(generatedId);

        return visitEntity;
    }

    @Override
    public List<VisitEntity> findAll() {
        return jdbcTemplate.query(SELECT_ALL_QUERY, (rs, rowNum) -> {
            final VisitEntity visitEntity = new VisitEntity();
            visitEntity.setId(rs.getLong("id"));
            visitEntity.setName(rs.getString("name"));
            visitEntity.setDate(rs.getDate("visit_date").toLocalDate());
            visitEntity.setMessage(rs.getString("message"));
            return visitEntity;
        });
    }


    @Override
    public void delete(Long id) {
        Objects.requireNonNull(id, "id can't be null!");
        jdbcTemplate.update(DELETE_QUERY, id);
    }
}
