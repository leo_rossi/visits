package br.edu.ifsp.visitas.repository;

import br.edu.ifsp.visitas.entity.VisitEntity;

import java.util.List;

public interface VisitRepository {

    VisitEntity save(final VisitEntity visitEntity);

    List<VisitEntity> findAll();

    void delete(final Long id);
}
