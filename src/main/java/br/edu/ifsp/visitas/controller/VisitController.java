package br.edu.ifsp.visitas.controller;

import java.util.List;

import javax.validation.Valid;

import br.edu.ifsp.visitas.entity.VisitEntity;
import br.edu.ifsp.visitas.repository.VisitRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class VisitController {

    private static final Logger log = LoggerFactory.getLogger(VisitController.class);

    @Autowired
    private VisitRepository visitRepository;

    @GetMapping
    public String getHome() {
        log.info("M=getHome, path='/'");
        return "home";
    }

    @PostMapping("/visit/save")
    public String save(@ModelAttribute("visit") @Valid final VisitEntity visitEntity, final Errors errors) {
        log.info("m=save, path='/visit/save', visitEntity={}, containErrors={}", visitEntity, errors.hasErrors());
        if (errors.hasErrors()) {
            log.warn("Form contains errors!");
            return "home";
        }
        visitRepository.save(visitEntity);
        return "redirect:/";
    }

    @GetMapping("/visit/{id}/delete")
    public String delete(@PathVariable("id") final Long id) {
        log.info("M=delete, path='/visit/{}/delete', id={}", id, id);
        visitRepository.delete(id);
        return "redirect:/";
    }

    @ModelAttribute(name = "visit")
    private VisitEntity visit() {
        return new VisitEntity();
    }

    @ModelAttribute(name = "visits")
    private List<VisitEntity> visits() {
        return visitRepository.findAll();
    }

}
