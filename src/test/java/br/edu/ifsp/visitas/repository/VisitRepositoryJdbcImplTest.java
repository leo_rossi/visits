package br.edu.ifsp.visitas.repository;

import br.edu.ifsp.visitas.entity.VisitEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.jdbc.JdbcTestUtils;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class VisitRepositoryJdbcImplTest {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private VisitRepository visitRepository;

    @Test
    public void save() {
        int count = JdbcTestUtils.countRowsInTable(jdbcTemplate, "visit");
        assertEquals(0, count);

        final VisitEntity visitEntity = new VisitEntity();
        visitEntity.setName("Usuário");

        VisitEntity persistedEntity = visitRepository.save(visitEntity);
        count = JdbcTestUtils.countRowsInTable(jdbcTemplate, "visit");

        assertNotNull(visitEntity.getId());
        assertNotNull(visitEntity.getDate());
        assertEquals(1, count);
    }

    @Test
    public void findAll() {
        int count = JdbcTestUtils.countRowsInTable(jdbcTemplate, "visit");
        List<VisitEntity> visits = visitRepository.findAll();

        assertEquals(count, visits.size());
    }

    @Test
    public void delete() {
        final VisitEntity visitEntity = new VisitEntity();
        visitEntity.setName("Usuário");
        VisitEntity persistedEntity = visitRepository.save(visitEntity);

        int count = JdbcTestUtils.countRowsInTable(jdbcTemplate, "visit");
        assertEquals(1, count);


        visitRepository.delete(persistedEntity.getId());
        count = JdbcTestUtils.countRowsInTable(jdbcTemplate, "visit");
        assertEquals(0, count);
    }
}
